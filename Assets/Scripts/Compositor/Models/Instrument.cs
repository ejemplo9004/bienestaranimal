﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class Instrument
{


 
    private int previusTab;
    private int actualPage; //actual Page who Contains value Text
    private int pagesNum; //Amount of Pages

    public List<Page> pagesData = new List<Page>();

    private List<Page> pages;

    public string name;

    public int numNotes;

    #region Getters and Setters
    public int ActualPage { get => actualPage; set => actualPage = value; }
    public int PreviusTab { get => previusTab; set => previusTab = value; }
    public int PagesNum { get => pagesNum; set => pagesNum = value; }
    public List<Page> Pages { get => pages; set => pages = value; }
    #endregion

    #region Constructors
    public Instrument() { }

    public Instrument(int pagesNum,string name,int numNotes)
    {
        this.numNotes = numNotes;
        this.name = name;
        this.PagesNum = pagesNum;
        CreatePages();
        pages[0] = new Page(true, 1);
        actualPage = 0;
        previusTab = 0;
    }

    public Instrument(string str)
    {
        DesdeString(str);
        PagesDataToPage();
        this.PagesNum = pagesData.Count;
        actualPage = 0;
        ResetPagesData();
    }

    #endregion

    #region CRUD
    public void DeletePages()
    {
       
        previusTab = actualPage;
        Pages[previusTab].IsActive = false;
        SetActualPage();
        Pages[actualPage].IsActive = true;
        PagesNum -= 1;

    }
    public void AddPages()
    {

        previusTab = actualPage;

        PagesNum += 1;

        for (int i = 0; i < Pages.Count; i++) //active first inactive page
        {
            if (!Pages[i].IsActive) {
                
                Pages[i].IsActive = true;
                
                Pages[i].Value = PagesNum;
                actualPage = i;
                break;
            }
        }

   

    }

    public void CreatePages()
    {
        pages = new List<Page>();
        for (int i = 0; i < CompositorUI.numTabs; i++)
        {
            Pages.Add( new Page(false,-1));
        }
        
    }
    public void SetActualPage()
    {
        if (actualPage == 9 || Pages[actualPage].Value == (PagesNum ))
        {
            for (int i = 0; i < Pages.Count; i++)
            {
                if (Pages[i].IsActive && (Pages[i].Value + 1) == Pages[actualPage].Value)
                {
                    actualPage = i;
                    return;
                }
            }
            actualPage -= 1; // No necesario
        }
        else {
            for (int i = 0; i < Pages.Count; i++)
            {
                if (Pages[i].IsActive && (Pages[i].Value-1)==Pages[actualPage].Value) {
                    actualPage = i;
                    return;
                }
            }
            actualPage += 1; // No necesario

        }
           
    }
    public void ResetPage(int i)
    {
        int value = Pages[i].Value;

        if (value > Pages[actualPage].Value) {
            value -= 1;
        }


        for (int x = actualPage; x < Pages.Count; x++)
        {
            if (Pages[x].IsActive)
            {
                Pages[x].Value = value;
                value++;
            }
        }

        if (i == PagesNum)
        {

            for (int x= actualPage-2; x>=0; x--)
            {
                if (Pages[x].IsActive) {
                    previusTab = x;
                    break;
                }
            }
        }
        else {
                      
            #region Useful(?)
            for (int x = actualPage - 1; x >= 0; x--)
            {
                if (Pages[x].IsActive)
                {
                    previusTab = x;
                    break;
                }
            }
            #endregion
        }

        Pages[i] = new Page(false,-1);
        
    }

    public Page GetPage(int index) {

        foreach (var page in Pages)
        {
            
            if (page.Value == index) {
                return page;
            }
        }

        return null;
    }
    public Page GetPageActive(int index)
    {

        foreach (var page in Pages)
        {

            if (page.Value == index && page.IsActive)
            {
                return page;
            }
        }

        return null;
    }


  
    #endregion

    #region Save
    public string HaciaString()
    {
        PagesToPagesData();
        string result = "";

        foreach (Page pagina in pagesData)
        {
            if (!result.Equals(""))
            {
                result += "+";
            }

            result += pagina.HaciaString();

        }
        ResetPagesData();

        return result;

    }

    public void PagesToPagesData()
    {
        //Sor page for value;
        for (int i = 0; i < Pages.Count; i++)
        {
            Page newPage = GetPageActive(i + 1);
            if (newPage != null)
                pagesData.Add(newPage);
        }
    }

    public void PagesDataToPage() {

        CreatePages();

        for (int i = 0; i < pagesData.Count; i++)
        {
            
            pages[i] = new Page(true, (i + 1));
            pages[i].Grid = pagesData[i].Grid;
            
        }

    }
    #endregion


    #region Load
    public void DesdeString(string str)
    {
        string[] arreglo;
        arreglo = str.Split('+');
        pagesData = new List<Page>();
        for (int i = 0; i < arreglo.Length; i++)
        {

            pagesData.Add(new Page(arreglo[i]));

        }


    }

    #endregion

    void ResetPagesData() {
        pagesData = new List<Page>();
    }
}
