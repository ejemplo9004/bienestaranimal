﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Compositor 
{
    private List<Instrument> instruments = new List<Instrument>();
    public static string trackName="";
    private int actualInstrument;
    private int biggerInstrument;

 


    #region Constructors
    public Compositor() { }
    public Compositor(int numInstruments)
    {

        CreateInstruments(numInstruments);

        actualInstrument = 0;
        BiggerInstrument = 1;
    }

    public Compositor(string str)
    {
        this.DesdeString(str);
       
    }
    #endregion

    #region Getters and Setters
    public  int BiggerInstrument { get => biggerInstrument; set => biggerInstrument = value; }
    public int ActualInstrument { get => actualInstrument; set => actualInstrument = value; }

    public List<Instrument> Instruments { get => instruments; set => instruments = value; }

    #endregion


    #region SaveData

    public string HaciaString()
    {
        string result = "";

        foreach (Instrument instrumento in instruments)
        {
            if (!result.Equals(""))
            {
                result += "|";
            }

            result += instrumento.HaciaString();

        }

        return result;

    }

    #endregion

    #region Load Data
    public void DesdeString(string str)
    {
        string[] arreglo;
        arreglo = str.Split('|');
        instruments = new List<Instrument>();
        for (int i = 0; i < arreglo.Length; i++)
        {

            instruments.Add(new Instrument(arreglo[i]));

        }

        actualInstrument = 0;
        UpdateBiggerInstrument();


    }

    #endregion

    #region Class Methods
   public  void CreateInstruments(int numInstruments)
    {
        int cont = 0;
        while (cont < numInstruments) { 
            instruments.Add(new Instrument(1,GameController.instance.instrumentProp[cont].name, GameController.instance.instrumentProp[cont].numNotes));//Configurate constructor
            cont++;
        }
    }

   public  void UpdateBiggerInstrument()
    {

        foreach (var instrument in instruments)
        {
            if (instrument.PagesNum > biggerInstrument)
            {
                biggerInstrument = instrument.PagesNum;
            }

        }

    }



    #endregion



}
