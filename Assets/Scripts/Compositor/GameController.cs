﻿using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public List<InstrumentPropierties> instrumentProp = new List<InstrumentPropierties>();

    [HideInInspector] public Compositor compositor;
    CompositorUI compositorUI;
    public static int numInstruments;


    
    public static GameController instance;

    #region Awake,Start...
    private void Awake()
    {

        instance = this;
        Load();
    }
 
    void Start()
    {
        compositorUI = CompositorUI.instance;
        numInstruments = compositorUI.instrumentsUI.Count;
    }
    #endregion

    #region Save And Load
    public void Save()
    {

        MorionTools.Guardar(Compositor.trackName, compositor.HaciaString());

        string nombrePistas = MorionTools.Cargar("nombrePistas");
        if (nombrePistas != null)
        {
            if (!nombrePistas.Contains(Compositor.trackName))
            {
                nombrePistas += Compositor.trackName + ",";

                MorionTools.Guardar("nombrePistas", nombrePistas);
            }
        }

    }





    public void Load()
    {
        compositor = new Compositor();

        string loadedData = MorionTools.Cargar(Compositor.trackName);

        if (loadedData.Equals("")) // New Compositor
        {
            compositor = new Compositor(instrumentProp.Count);  
            return;
        }

        compositor.DesdeString(loadedData); //Load compositor data
    }

    #endregion

    [System.Serializable]
    public class InstrumentPropierties
    {
        public string name;
        public int numNotes;

        public InstrumentPropierties(string name, int numNotes)
        {
            this.name = name;
            this.numNotes = numNotes;

        }
    }

}
